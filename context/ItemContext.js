import React, {createContext, useContext, useState} from "react";

const ItemContext = createContext();

export function useItemContext() {
    return useContext(ItemContext);
}

export function ItemProvider({children}) {

    const [playing, setPlaying] = useState(false);
    const [progress, setProgress] = useState(0);
    const [position, setPosition] = useState(0);

    const value = {
        playing,
        setPlaying,
        progress,
        setProgress,
        position,
        setPosition
    };

    return <ItemContext.Provider value={value}>{children}</ItemContext.Provider>;
}