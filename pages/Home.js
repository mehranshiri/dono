import React, { useState, useEffect } from 'react';
import { SafeAreaView, FlatList, StyleSheet, View } from 'react-native';
import Constants from 'expo-constants';
import * as manifest from '../data/manifest.json';
import ListItem from '../components/ListItems';
import { getAll, readData, saveData } from '../helpers/AsyncHelper';
import { FAVORITE_KEY, RATE_KEY } from '../config/Const';
import { useIsFocused } from "@react-navigation/native";

export default function Home({ navigation }) {

  const [favoriteValue, setFavoriteValue] = useState('');
  const [rates, setRates] = useState({});
  const isFocused = useIsFocused();

  useEffect(() => {
    readData(FAVORITE_KEY)
    .then(val => setFavoriteValue(val))

    readData(RATE_KEY)
    .then(res => {
      setRates(JSON.parse(res))
    })
  }, [isFocused, favoriteValue])
  
  function handleFavoriteClick(value) {
    saveData(FAVORITE_KEY, value)
    .then(() => setFavoriteValue(value))
  }

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.list}>
        <FlatList
          data={manifest.data}
          renderItem={({ item }) => 
            <ListItem 
              handleNavigation={() => navigation.navigate('Item', {
                item: item
              })}
              handleFavoriteClick={handleFavoriteClick} 
              isFav={favoriteValue == item.totalDurationMs}
              item={item}
              rate={rates[item.totalDurationMs] ? rates[item.totalDurationMs] : 0}
            />
          }
          keyExtractor={item => item.totalDurationMs}
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 5
  },
  list: {
    marginTop: Constants.statusBarHeight,
  }
});