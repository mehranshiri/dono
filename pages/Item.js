import React, { useState, useEffect } from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity,  } from 'react-native';
import Progressbar from '../components/Progressbar';
import Rating from '../components/Rating';
import SoundPlayer from '../components/SoundPlayer';
import { FAVORITE_KEY, RATE_KEY } from '../config/Const';
import { readData, saveData } from '../helpers/AsyncHelper';
import { ItemProvider } from '../context/ItemContext';
const lineHeart = '../assets/icons/heart/heart-line-black.png';
const filledHeart = '../assets/icons/heart/heart-filled-black.png';

export default function Item({ route, navigation }) {

  const { item: {title, cover, audio, totalDurationMs} } = route.params;

  const [favoriteValue, setFavoriteValue] = useState('');
  const [rating, setRating] = useState(0);

  useEffect(() => {
    readData(RATE_KEY)
    .then(res => {
      let rates = JSON.parse(res)
      if(rates && rates[totalDurationMs]) {
        setRating(rates[totalDurationMs])
      }
    })

    readData(FAVORITE_KEY)
    .then(val => setFavoriteValue(val))
  }, []);

  async function handleRate(value) {
    readData(RATE_KEY)
      .then(res => {
        let rates = JSON.parse(res);
        if(!rates) {
          rates = {};
        }
        rates[totalDurationMs] = value;
        saveData(RATE_KEY, JSON.stringify(rates))
        .then(() => setRating(value))
      })
  }

  function handleFavoriteClick(value) {
    saveData(FAVORITE_KEY, value)
    .then(() => setFavoriteValue(value))
  }

  return (
    <ItemProvider>
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.coverBox}>
          <Image
            style={styles.cover}
            source={{uri: cover}}
          />
          <SoundPlayer source={audio} />
          <TouchableOpacity onPress={() => handleFavoriteClick(totalDurationMs.toString())}>
              <Image
                style={styles.heart}
                source={favoriteValue == totalDurationMs ? require(filledHeart) : require(lineHeart)}
              />
          </TouchableOpacity>
        </View>
        <View style={styles.meta}>
            <Progressbar total={totalDurationMs} />
            <Rating rate={rating} handlePress={handleRate} style={{ rating: styles.rating, size: 70 }} />
        </View>
      </View>
    </ItemProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15
  },
  title: {
    textAlign: 'center',
    fontSize: 28,
    paddingVertical: 25
  },
  coverBox: {
    display: 'flex',
    position: 'relative',
    height: 350
  },
  cover: {
    width: '100%',
    height: 350,
  },
  heart: {
    display: 'flex',
    alignSelf: 'flex-end',    
    position: 'absolute',
    top: -50,
    right: 0,
    width: 50,
    height: 50,
  },
  meta: {
    display: 'flex',
    flexDirection: 'column'
  },
  rating: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      paddingTop: 25
  },
});