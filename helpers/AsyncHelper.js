import AsyncStorage from '@react-native-async-storage/async-storage';

export const saveData = async (STORAGE_KEY, value) => {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, value)
    } catch (e) {
      alert('Failed to save the data to the storage')
    }
}

export const readData = async STORAGE_KEY => {
    try {
      const value = await AsyncStorage.getItem(STORAGE_KEY);
  
      if (value !== null) {
        return value;
      }
      return null;

    } catch (e) {
      alert('Failed to fetch the input from storage');
    }
};

export const removeData = async STORAGE_KEY => {
    try {
      await AsyncStorage.removeItem(STORAGE_KEY);
      alert('Storage successfully cleared!');
    } catch (e) {
      alert('Failed to clear the async storage.');
    }
};