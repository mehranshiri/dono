import React, { useEffect, useRef } from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Audio } from 'expo-av';
import { useItemContext } from '../context/ItemContext';
const play = '../assets/icons/audio/play.png';
const pause = '../assets/icons/audio/pause.png';

export default function SoundPlayer({ source }) {

  const sound = useRef(new Audio.Sound());
  
  const itemContext = useItemContext()

  async function playSound() {
    try {
      const result = await sound.current.getStatusAsync();
      if (result.isLoaded) {
        console.log('play');
        if (result.isPlaying === false) {
          sound.current.playAsync();
          itemContext.setPlaying(true);
        }
      }
    } catch (error) {}
  }

  async function pauseSound() {
    try {
      const result = await sound.current.getStatusAsync();
      if (result.isLoaded) {
        if (result.isPlaying === true) {
          sound.current.pauseAsync();
          itemContext.setPlaying(false);
        }
      }
    } catch (error) {}
  }

  useEffect(() => {
    loadAudio();
    return() => {
      sound.current.stopAsync();
    }
  }, [])

  const loadAudio = async () => {
    const checkLoading = await sound?.current.getStatusAsync();
    if (checkLoading.isLoaded === false) {
      try {
        const result = await sound.current.loadAsync({uri: source}, {}, true);
        if (result.isLoaded === false) {
          console.log('Error in Loading Audio');
        } 
      } catch (error) {
        console.log('ts');
      }
    }
  };

  useEffect(() => {
    if(itemContext.progress != itemContext.position) {
      handlePositionChange();
    }
  }, [itemContext.position]);

  async function handlePositionChange() {
    try {
      const result = await sound.current.getStatusAsync();
      if (result.isLoaded) {
        itemContext.setProgress(itemContext.position)
        sound.current.setPositionAsync(Math.floor(itemContext.position))
      }
    } catch(error) { 
      console.log(error) 
    }
  }

  return (
    <TouchableOpacity style={styles.playBtn} onPress={() => !itemContext.playing ? playSound() : pauseSound()}>
      <Image
        style={styles.playImg}
        source={itemContext.playing ? require(pause) : require(play)}
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  playBtn: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: [{ translateY: -50 }, { translateX: -50 }],
  },
  playImg: {
    width: 100,
    height: 100
  },
});