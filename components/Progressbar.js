import React, { useEffect } from "react"
import { SafeAreaView, StyleSheet, View, Text } from 'react-native'
import Slider from '@react-native-community/slider';
import { useItemContext } from "../context/ItemContext";
import { millisToMinutesAndSeconds } from "../helpers/TimeConvert";

export default function Progressbar({ total }) {

    const itemContext = useItemContext();

    useEffect(() => {
        if (itemContext.progress < total && itemContext.playing) {
            const timeInternal = setInterval(() => itemContext.setProgress((progress) => progress + 100), 100)
            return () => clearTimeout(timeInternal)
        }
    }, [itemContext.playing, itemContext.progress])

    function handleValueChange(val) {
        itemContext.setPosition(val);
    }

    return (
    <SafeAreaView style={styles.progressbar}>
        <Slider
            style={styles.slider}
            value={itemContext.progress}
            minimumValue={0}
            maximumValue={total}
            minimumTrackTintColor={'#0e0e0e'}
            onValueChange={handleValueChange}
        />
        <View style={styles.TimeBox}>
            <Text style={styles.time}>{millisToMinutesAndSeconds(itemContext.progress)} / {millisToMinutesAndSeconds(total)}</Text>
        </View>
    </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    progressbar: {
        width: '100%',
        marginVertical: 30
    },
    slider: {
        width: '100%',
        height: 10,
        backgroundColor: '#eee',
    },
    TimeBox: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: 15,
        marginHorizontal: 'auto'
    },
    time: {
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    }
})