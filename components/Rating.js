import React, { useEffect, useState } from 'react';
import { View, Image, StyleSheet, TouchableHighlight, Text } from 'react-native';

export default function Rating({ rate, handlePress, style: {rating, size} }) {
        
    const [stars, setStars] = useState([]);
    
    useEffect(() => {
        let content = [];
        for(var i=0; i<5; i++) {
            if(i < rate) {
                content.push(true)
            } else {
                content.push(false)
            }
        }
        setStars(content);
    }, [rate]);

    return (
        <View style={rating} >
            {stars.map((star, key) => 
            <TouchableHighlight key={key} onPress={() => handlePress(key+1)}>
                <Image
                    style={styles.star(size)}
                    source={star ? require('../assets/icons/star/star-filled-black.png') : require('../assets/icons/star/star-line-black.png')}
                />
            </TouchableHighlight>
        )}            
        </View>
    )
}

const styles = StyleSheet.create({
    star: size => ({
        width: size,
        height: size
    })
})

Rating.defaultProps = {
    rate: 0,
    handlePress: {},
    size: 25
}