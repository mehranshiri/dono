import { View, Image, StyleSheet, Text, TouchableHighlight, TouchableOpacity, SafeAreaView } from 'react-native';
import Rating from './Rating';

export default function ListItem({ item, handleFavoriteClick, isFav, handleNavigation, rate }) {
    const {title, cover, totalDurationMs } = item;
    const lineHeart = '../assets/icons/heart/heart-line-black.png';
    const filledHeart = '../assets/icons/heart/heart-filled-black.png';
    return (
      <TouchableOpacity
        onPress={handleNavigation}
      >
        <View style={styles.item}>
          <Rating style={{ rating: styles.rating, size: 25 }} rate={rate} handlePress={() => {}} />
          <Image
            style={styles.cover}
            source={{uri: cover}}
          />
          <View style={styles.meta}>
              <TouchableHighlight onPress={() => handleFavoriteClick(totalDurationMs.toString())}>
                  <Image
                      style={styles.heart}
                      source={isFav ? require(filledHeart) : require(lineHeart)}
                  />
              </TouchableHighlight>
              <SafeAreaView style={styles.titleBox}>
                <Text style={styles.title}>{title}</Text>
              </SafeAreaView>
          </View>
        </View>
      </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
  item: {
    flex: 1,
    backgroundColor: '#eee',
    marginVertical: 10,
    paddingBottom: 5,
    borderWidth: 1,
    borderColor: '#000000'
  },
  cover: {
    flex: 1,
    width: null,
    height: 250,
    zIndex: 0
  },
  meta: {
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
    alignItems: 'center',
    direction: 'rtl',
    height: 60,
    padding: 10
  },
  heart: {
    width: 40,
    height: 40
  },
  titleBox: {
    position: 'absolute',
    left: 0,
    right: 0,
    marginHorizontal: 'auto',
    textAlign: 'center'
  },
  title: {
    fontSize: 28,
    textAlign: 'center'
  },
  rating: {
    position: 'absolute',
    top: 25,
    left: 10,
    zIndex: 9,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  }
});